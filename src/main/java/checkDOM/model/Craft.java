package checkDOM.model;

public class Craft {
  private String model;
  private String type;
  private Integer crew;
  private String mainWeapon;
  private String secondaryWeapon;
  private String powerplants;
  private Integer speed;
  private Integer range;
  private Double weight;
  private Double length;
  private Double wingspawn;
  private Double height;
  private String origin;

  public Craft() {
  }

  public Craft(String model, String type, Integer crew, String mainWeapon,
    String secondaryWeapon, String powerplants, Integer speed, Integer range,
    Double weight, Double length, Double wingspawn, Double height,
    String origin) {
    this.model = model;
    this.type = type;
    this.crew = crew;
    this.mainWeapon = mainWeapon;
    this.secondaryWeapon = secondaryWeapon;
    this.powerplants = powerplants;
    this.speed = speed;
    this.range = range;
    this.weight = weight;
    this.length = length;
    this.wingspawn = wingspawn;
    this.height = height;
    this.origin = origin;
  }
  public String getModel() {
    return model;
  }
  public String getType() {
    return type;
  }
  public Integer getCrew() {
    return crew;
  }
  public String getMainWeapon() {
    return mainWeapon;
  }
  public String getSecondaryWeapon() {
    return secondaryWeapon;
  }
  public String getPowerplants() {
    return powerplants;
  }
  public Integer getSpeed() {
    return speed;
  }
  public Integer getRange() {
    return range;
  }
  public Double getWeight() {
    return weight;
  }
  public Double getLength() {
    return length;
  }
  public Double getWingspawn() {
    return wingspawn;
  }
  public Double getHeight() {
    return height;
  }
  public String getOrigin() {
    return origin;
  }

  public void setModel(String model) {
    this.model = model;
  }

  public void setType(String type) {
    this.type = type;
  }

  public void setCrew(Integer crew) {
    this.crew = crew;
  }

  public void setMainWeapon(String mainWeapon) {
    this.mainWeapon = mainWeapon;
  }

  public void setSecondaryWeapon(String secondaryWeapon) {
    this.secondaryWeapon = secondaryWeapon;
  }

  public void setPowerplants(String powerplants) {
    this.powerplants = powerplants;
  }

  public void setSpeed(Integer speed) {
    this.speed = speed;
  }

  public void setRange(Integer range) {
    this.range = range;
  }

  public void setWeight(Double weight) {
    this.weight = weight;
  }

  public void setLength(Double length) {
    this.length = length;
  }

  public void setWingspawn(Double wingspawn) {
    this.wingspawn = wingspawn;
  }

  public void setHeight(Double height) {
    this.height = height;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }
}
