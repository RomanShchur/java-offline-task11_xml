package checkDOM.view;

import checkDOM.model.Craft;

public class UiDom {
  public static void outCraftObjValues(Craft craft) {
    System.out.println("Craft Model = " + craft.getModel());
    System.out.println("Craft type = " + craft.getType());
    System.out.println("Craft crew = " + craft.getCrew());
    System.out.println("Craft main_weapon = " + craft.getMainWeapon());
    System.out.println("Craft secondary_weapon = " + craft.getSecondaryWeapon());
    System.out.println("Craft powerplants = " + craft.getPowerplants());
    System.out.println("Craft speed = " + craft.getSpeed());
    System.out.println("Craft range = " + craft.getRange());
    System.out.println("Craft weight = " + craft.getWeight());
    System.out.println("Craft length = " + craft.getLength());
    System.out.println("Craft wingspawn = " + craft.getWingspawn());
    System.out.println("Craft height = " + craft.getHeight());
    System.out.println("Craft Origin World = " + craft.getOrigin());
    System.out.println();
  }
}
