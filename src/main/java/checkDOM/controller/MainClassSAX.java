package checkDOM.controller;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import checkDOM.model.Craft;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;

import static checkDOM.view.UiDom.outCraftObjValues;

public class MainClassSAX {
  private static ArrayList<Craft> crafts = new ArrayList<>();

  public static void main(String[] args) {
    try {
      File inputFile = new File("E:\\java-offline-task11_XML"
        + "\\src\\main\\resources\\M40CraftsXML\\M40_Crafts.xml");
      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();
      UserHandler userhandler = new UserHandler();
      saxParser.parse(inputFile, userhandler);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
