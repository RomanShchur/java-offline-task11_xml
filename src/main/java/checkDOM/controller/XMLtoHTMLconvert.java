package checkDOM.controller;

import java.io.FileOutputStream;
import java.io.OutputStream;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
public class XMLtoHTMLconvert {
  public static void main(String[] args) throws Exception {
    try {
      TransformerFactory tFactory = TransformerFactory.newInstance();
      Source xslDoc = new StreamSource("E:\\java-offline-task11_XML"
        + "\\src\\main\\resources\\M40CraftsXML\\M40_Crafts.xsl");
      Source xmlDoc = new StreamSource("E:\\java-offline-task11_XML"
        + "\\src\\main\\resources\\M40CraftsXML\\M40_Crafts.xml");
      String outputFileName = "E:\\java-offline-task11_XML"
        + "\\src\\main\\resources\\M40CraftsXML\\M40_Crafts.html";
      OutputStream htmlFile = new FileOutputStream(outputFileName);
      Transformer trasform = tFactory.newTransformer(xslDoc);
      trasform.transform(xmlDoc, new StreamResult(htmlFile));
    } catch (Exception e)
    {
      e.printStackTrace();
    }
  }
}
