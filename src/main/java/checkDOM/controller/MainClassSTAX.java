package checkDOM.controller;

import checkDOM.model.Craft;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class MainClassSTAX {
  public static void main(String[] args) {
    String fileName = "E:\\java-offline-task11_XML"
      + "\\src\\main\\resources\\M40CraftsXML\\M40_Crafts.xml";
    List<Craft> craftList = parseXML(fileName);
    for(Craft emp : craftList){
      System.out.println(emp.toString());
    }
  }

  private static List<Craft> parseXML(String fileName) {
    List<Craft> craftList = new ArrayList<>();
    Craft craft = null;
    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new FileInputStream(fileName));
      while(xmlEventReader.hasNext()){
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()){
          StartElement startElement = xmlEvent.asStartElement();
          if(startElement.getName().getLocalPart().equals("craft")){
            craft = new Craft();
          }
          else if(startElement.getName().getLocalPart().equals("model")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setModel(xmlEvent.asCharacters().getData());
          }else if(startElement.getName().getLocalPart().equals("type")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setType(xmlEvent.asCharacters().getData());
          }else if(startElement.getName().getLocalPart().equals("crew")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setCrew(Integer.valueOf(xmlEvent.asCharacters().getData()));
          }else if(startElement.getName().getLocalPart().equals("main_weapon")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setMainWeapon(xmlEvent.asCharacters().getData());
          } else if(startElement.getName().getLocalPart().equals("secondary_weapon")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setSecondaryWeapon(xmlEvent.asCharacters().getData());
          } else if(startElement.getName().getLocalPart().equals("powerplants")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setPowerplants(xmlEvent.asCharacters().getData());
          } else if(startElement.getName().getLocalPart().equals("speed")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setSpeed(Integer.valueOf(xmlEvent.asCharacters().getData()));
          } else if(startElement.getName().getLocalPart().equals("range")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setRange(Integer.valueOf(xmlEvent.asCharacters().getData()));
          } else if(startElement.getName().getLocalPart().equals("weight")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setWeight(Double.valueOf(xmlEvent.asCharacters().getData()));
          } else if(startElement.getName().getLocalPart().equals("length")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setLength(Double.valueOf(xmlEvent.asCharacters().getData()));
          } else if(startElement.getName().getLocalPart().equals("wingspawn")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setWingspawn(Double.valueOf(xmlEvent.asCharacters().getData()));
          } else if(startElement.getName().getLocalPart().equals("height")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setHeight(Double.valueOf(xmlEvent.asCharacters().getData()));
          } else if(startElement.getName().getLocalPart().equals("origin")){
            xmlEvent = xmlEventReader.nextEvent();
            craft.setOrigin(xmlEvent.asCharacters().getData());
          }
        }
        if(xmlEvent.isEndElement()){
          EndElement endElement = xmlEvent.asEndElement();
          if(endElement.getName().getLocalPart().equals("Employee")){
            craftList.add(craft);
          }
        }
      }

    } catch (FileNotFoundException | XMLStreamException e) {
      e.printStackTrace();
    }
    return craftList;
  }
}
