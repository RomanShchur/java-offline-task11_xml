package checkDOM.controller;

import checkDOM.model.Craft;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import java.util.ArrayList;

import static checkDOM.view.UiDom.outCraftObjValues;

public class MainClassDOM {
  private static ArrayList<Craft> crafts = new ArrayList<>();

  public static void main(String[] args) {
    try {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(new File("E:\\java-offline-task11_XML"
        + "\\src\\main\\resources\\M40CraftsXML\\M40_Crafts.xml"));
      NodeList craftNodes = document.getElementsByTagName("craft");
      for (int i = 0; i < craftNodes.getLength(); i++) {
        Node craftNode = craftNodes.item(i);
        if (craftNode.getNodeType() == Node.ELEMENT_NODE) {
          Element craftElement = (Element) craftNode;
          String craftModel =
            craftElement.getElementsByTagName("model").item(0).getTextContent();
          String craftType =
            craftElement.getElementsByTagName("type").item(0).getTextContent();
          String craftCrew =
            craftElement.getElementsByTagName("crew").item(0).getTextContent();
          String main_weapon =
            craftElement.getElementsByTagName("main_weapon").item(0).getTextContent();
          String secondary_weapon =
            craftElement.getElementsByTagName("secondary_weapon").item(0).getTextContent();
          String powerplants =
            craftElement.getElementsByTagName("powerplants").item(0).getTextContent();
          String speed =
            craftElement.getElementsByTagName("speed").item(0).getTextContent();
          String range =
            craftElement.getElementsByTagName("range").item(0).getTextContent();
          String weight =
            craftElement.getElementsByTagName("weight").item(0).getTextContent();
          String length =
            craftElement.getElementsByTagName("length").item(0).getTextContent();
          String wingspawn =
            craftElement.getElementsByTagName("wingspawn").item(0).getTextContent();
          String height =
            craftElement.getElementsByTagName("height").item(0).getTextContent();
          String craftOrigin =
            craftElement.getElementsByTagName("origin").item(0).getTextContent();
          crafts.add(new Craft(craftModel, craftType,
            Integer.valueOf(craftCrew), main_weapon, secondary_weapon,
            powerplants, Integer.valueOf(speed), Integer.valueOf(range),
            Double.valueOf(weight), Double.valueOf(length),
            Double.valueOf(wingspawn), Double.valueOf(height), craftOrigin)
          );
          outCraftObjValues(crafts.get(i));
        }
      }
    } catch (SAXException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (ParserConfigurationException e) {
      e.printStackTrace();
    }
  }
}
