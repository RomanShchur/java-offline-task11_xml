package checkDOM.controller;

import checkDOM.model.Craft;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

import static checkDOM.view.UiDom.outCraftObjValues;

class UserHandler extends DefaultHandler {
  private static ArrayList<Craft> crafts = new ArrayList<>();
  boolean bModel = false;
  boolean bType = false;
  boolean bCrew = false;
  boolean bMain_weapon = false;
  boolean bSecondary_weapon = false;
  boolean bPowerplants = false;
  boolean bSpeed = false;
  boolean bRange = false;
  boolean bWeight = false;
  boolean bLength = false;
  boolean bWingspawn = false;
  boolean bHeight = false;
  boolean bOrigin = false;

  @Override
  public void startElement(
    String uri, String localName, String qName, Attributes attributes)
    throws SAXException {

    if (qName.equalsIgnoreCase("model")) {
      bModel = true;
    } else if (qName.equalsIgnoreCase("type")) {
      bType = true;
    } else if (qName.equalsIgnoreCase("crew")) {
      bCrew = true;
    } else if (qName.equalsIgnoreCase("main_weapon")) {
      bMain_weapon = true;
    } else if (qName.equalsIgnoreCase("secondary_weapon")) {
      bSecondary_weapon = true;
    } else if (qName.equalsIgnoreCase("powerplants")) {
      bPowerplants = true;
    } else if (qName.equalsIgnoreCase("speed")) {
      bSpeed = true;
    } else if (qName.equalsIgnoreCase("range")) {
      bRange = true;
    } else if (qName.equalsIgnoreCase("weight")) {
      bWeight = true;
    } else if (qName.equalsIgnoreCase("length")) {
      bLength = true;
    } else if (qName.equalsIgnoreCase("wingspawn")) {
      bWingspawn = true;
    } else if (qName.equalsIgnoreCase("height")) {
      bHeight = true;
    } else if (qName.equalsIgnoreCase("origin")) {
      bOrigin = true;
    }
  }
  @Override
  public void endElement(String uri,
    String localName, String qName) throws SAXException {

    if (qName.equalsIgnoreCase("craft")) {
      System.out.println("End Element :" + qName);
    }
  }
  @Override
  public void characters(char ch[], int start, int length) throws SAXException {
    String model = "";
    String type = "";
    Integer crew = 0;
    String main_weapon = "";
    String secondary_weapon = "";
    String powerplants = "";
    Integer speed = 0;
    Integer range = 0;
    Double weight = 0.0;
    Double craftLength = 0.0;
    Double wingspawn = 0.0;
    Double height = 0.0;
    String origin = "";
    if (bModel) {
      model = new String(ch, start, length);
      System.out.println("Model: " + new String(ch, start, length));
      bModel = false;
    } else if (bType) {
      type = new String(ch, start, length);
      System.out.println("Type: " + new String(ch, start, length));
      bType = false;
    } else if (bCrew) {
      crew = Integer.valueOf(new String(ch, start, length));
      System.out.println("crew: " + new String(ch, start, length));
      bCrew = false;
    } else if (bMain_weapon) {
      main_weapon = new String(ch, start, length);
      System.out.println("Main_weapon: " + new String(ch, start, length));
      bMain_weapon = false;
    } else if (bSecondary_weapon) {
      secondary_weapon = new String(ch, start, length);
      System.out.println("Secondary_weapon: " + new String(ch, start, length));
      bSecondary_weapon = false;
    } else if (bPowerplants) {
      powerplants = new String(ch, start, length);
      System.out.println("Powerplants: " + new String(ch, start, length));
      bPowerplants = false;
    } else if (bSpeed) {
      speed = Integer.valueOf(new String(ch, start, length));
      System.out.println("Speed: " + new String(ch, start, length));
      bSpeed = false;
    } else if (bRange) {
      range = Integer.valueOf(new String(ch, start, length));
      System.out.println("Range: " + new String(ch, start, length));
      bRange = false;
    } else if (bWeight) {
      weight = Double.valueOf(new String(ch, start, length));
      System.out.println("Weight: " + new String(ch, start, length));
      bWeight = false;
    } else if (bLength) {
      craftLength = Double.valueOf(new String(ch, start, length));
      System.out.println("Length: " + new String(ch, start, length));
      bLength = false;
    } else if (bWingspawn) {
      wingspawn = Double.valueOf(new String(ch, start, length));
      System.out.println("Wingspawn: " + new String(ch, start, length));
      bWingspawn = false;
    } else if (bHeight) {
      height = Double.valueOf(new String(ch, start, length));
      System.out.println("Height: " + new String(ch, start, length));
      bHeight = false;
    } else if (bOrigin) {
      origin = new String(ch, start, length);
      System.out.println("Origin: " + new String(ch, start, length));
      bOrigin = false;
    }
    crafts.add(new Craft(model, type, crew, main_weapon, secondary_weapon,
      powerplants, speed, range, weight, craftLength, wingspawn, height, origin));
    for (int i = 0; i < crafts.size(); i++) {
      outCraftObjValues(crafts.get(i));
    }
  }
}
