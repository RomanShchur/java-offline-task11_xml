<?xml version="1.0" encoding="windows-1251"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  <xsl:output method="html"/>
  <xsl:template match="/">
    <html>
      <head>
        <title>M40_Crafts.xsl</title>
      </head>
      <body>
        <h2>M40 Spacecrafts</h2>
        <table border="1">
          <tr bgcolor="#87CEEB">
            <th>Craft Model</th>
            <th>Craft Type</th>
            <th>Craft Crew</th>

            <th>Main Weapon</th>
            <th>Seconary Weapon</th>

            <th>Powerplants</th>
            <th>Speed</th>
            <th>Range</th>

            <th>Weigth</th>
            <th>Length</th>
            <th>Wingspawn</th>
            <th>Height</th>

            <th>Origin</th>
          </tr>
          <xsl:for-each select="root/crafts/craft">
            <tr>
              <td><xsl:value-of select="model"/></td>
              <xsl:for-each select="chars">
                <td><xsl:value-of select="type"/></td>
                <td><xsl:value-of select="crew"/></td>
                <xsl:for-each select="weaponary">
                  <td><xsl:value-of select="main_weapon"/></td>
                  <td><xsl:value-of select="secondary_weapon"/></td>
                </xsl:for-each>
              </xsl:for-each>
              <xsl:for-each select="params">
                <xsl:for-each select="functional">
                  <td><xsl:value-of select="powerplants"/></td>
                  <td><xsl:value-of select="speed"/></td>
                  <td><xsl:value-of select="range"/></td>
                </xsl:for-each>
                <xsl:for-each select="non_functional">
                  <td><xsl:value-of select="weigth"/></td>
                  <td><xsl:value-of select="length"/></td>
                  <td><xsl:value-of select="wingspawn"/></td>
                  <td><xsl:value-of select="height"/></td>
                </xsl:for-each>
              </xsl:for-each>
              <td><xsl:value-of select="origin"/></td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
